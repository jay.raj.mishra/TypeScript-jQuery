﻿
class Customer {
    Id: number;
    IsActive: boolean;
    Age: number;
    Name?: string;
    Gender?: string;
    Company?: string;
    Email?: string;
    Phone?: string;
    Address?: string;
    Location?: string;

    constructor(responseData: Customer) {
        $.extend(this, responseData);
    }
}

class sum {
    x: number;
    y: number;

    constructor(responseData: sum) {
        $.extend(this, responseData);
    }
}

//Example to Add Sum of Number
function addsum(a: number, b: number) {
    var s: number = 0;
    $.ajax({
        type: 'POST',
        url: "", // API URL
        dataType: 'json',
        async: false,
        cache: false,
        data: {
            x: a,
            y: b
        },
        success: function (data) {
            var s = new sum(data);
            s = data.x + s.x + s.y;
        }
    });
}

function BindData() {

    $.ajax({
        type: 'GET',
        url: "/db.json",
        dataType: 'json',
        async: false,
        cache: false,
        success: function (data) {
            var $data = data.d;

            if ($data != null && $data != '') {
                var customer = new Customer($data);
                var strHtml = '';

                strHtml += '<table class="table">';
                strHtml += '<thead>';
                strHtml += '<tr>';
                strHtml += '<th>Id</th>';
                strHtml += '<th>IsActive</th>';
                strHtml += '<th>Age</th>';
                strHtml += '<th>Name</th>';
                strHtml += '<th>Gender</th>'
                strHtml += '<th>Company</th>';
                strHtml += '<th>Email</th>';
                strHtml += '<th>Phone</th>';
                strHtml += '<th>Address</th>'
                strHtml += '</tr>';
                strHtml += '</thead>';
                strHtml += '<tbody>';

                $.each(customer, function (index: number, item: Customer) {
                    strHtml += '<tr>';
                    strHtml += '<td>' + item.Id + '</td>';
                    strHtml += '<td>' + item.IsActive + '</td>';
                    strHtml += '<td>' + item.Age + '</td>';
                    strHtml += '<td>' + item.Name + '</td>';
                    strHtml += '<td>' + item.Gender + '</td>';
                    strHtml += '<td>' + item.Company + '</td>';
                    strHtml += '<td>' + item.Email + '</td>';
                    strHtml += '<td>' + item.Phone + '</td>';
                    strHtml += '<td>' + item.Address + '</td>';
                    strHtml += '</tr>';
                });
                strHtml += '</tbody>';
                strHtml += '</table>';

                $("#CustomerList").html("");
                $("#CustomerList").html(strHtml);
            }
        }
    });
}

