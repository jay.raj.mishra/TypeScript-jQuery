var Customer = /** @class */ (function () {
    function Customer(responseData) {
        $.extend(this, responseData);
    }
    return Customer;
}());
var sum = /** @class */ (function () {
    function sum(responseData) {
        $.extend(this, responseData);
    }
    return sum;
}());
//Example to Add Sum of Number
function addsum(a, b) {
    var s = 0;
    $.ajax({
        type: 'POST',
        url: "",
        dataType: 'json',
        async: false,
        cache: false,
        data: {
            x: a,
            y: b
        },
        success: function (data) {
            var s = new sum(data);
            s = data.x + s.x + s.y;
        }
    });
}
function BindData() {
    $.ajax({
        type: 'GET',
        url: "/db.json",
        dataType: 'json',
        async: false,
        cache: false,
        success: function (data) {
            var $data = data.d;
            if ($data != null && $data != '') {
                var customer = new Customer($data);
                var strHtml = '';
                strHtml += '<table class="table">';
                strHtml += '<thead>';
                strHtml += '<tr>';
                strHtml += '<th>Id</th>';
                strHtml += '<th>IsActive</th>';
                strHtml += '<th>Age</th>';
                strHtml += '<th>Name</th>';
                strHtml += '<th>Gender</th>';
                strHtml += '<th>Company</th>';
                strHtml += '<th>Email</th>';
                strHtml += '<th>Phone</th>';
                strHtml += '<th>Address</th>';
                strHtml += '</tr>';
                strHtml += '</thead>';
                strHtml += '<tbody>';
                $.each(customer, function (index, item) {
                    strHtml += '<tr>';
                    strHtml += '<td>' + item.Id + '</td>';
                    strHtml += '<td>' + item.IsActive + '</td>';
                    strHtml += '<td>' + item.Age + '</td>';
                    strHtml += '<td>' + item.Name + '</td>';
                    strHtml += '<td>' + item.Gender + '</td>';
                    strHtml += '<td>' + item.Company + '</td>';
                    strHtml += '<td>' + item.Email + '</td>';
                    strHtml += '<td>' + item.Phone + '</td>';
                    strHtml += '<td>' + item.Address + '</td>';
                    strHtml += '</tr>';
                });
                strHtml += '</tbody>';
                strHtml += '</table>';
                $("#CustomerList").html("");
                $("#CustomerList").html(strHtml);
            }
        }
    });
}
//# sourceMappingURL=Customer.js.map